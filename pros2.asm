code segment
assume cs:code

org 100h
main proc far
push ds
mov ax,0h
push ax

start:
mov ah,7
int 21h
p0:
cmp al,30h
js exit
cmp al,39h
ja p1
jmp outa

p1:
cmp al,41h
js exit
cmp al,46h
ja p2
sub al,11h
jmp aute

p2:
cmp al,61h
js exit
cmp al,66h
ja exit
sub al,31h
jmp aute

aute:
push ax
mov dl,31h
mov ah,2
int 21h

pop ax
outa:
mov dl,al
mov ah,2
int 21h

mov al,'.'
mov dl,al
mov ah,2
int 21h
jmp start
exit:
ret
main endp
code ends
end main