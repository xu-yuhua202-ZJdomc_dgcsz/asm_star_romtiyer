stck segment stack
db 80 dup (0)
stck ends

data segment
numf dw 0
missf db 12 dup (0)
Lsend db '$'
data ends

code segment
assume cs:code,ds:data,ss:stck
main proc far
push ds

mov ax,0
push ax

mov ax,80h
mov si,ax

mov ax,data
mov es,ax

mov ax,0
mov al,ds:[si]
inc si
mov di,offset numf
mov [di],ax
mov cx,numf

mov di,offset missf
rep movsb
mov ax,es
mov ds,ax

mov bx,1
mov cx,ax
mov dx,offset missf
mov ah,40h
int 21h

ret
main endp
code ends
end main