stck segment stack
db 33 dup (0)
stck ends

data segment
org 100h
NumFTtl = This word
numft db 0,?
MunTGet = This word
muntg db ?,?
nNMnuer label byte
wNUmeer dw 0000h
buff db 30h,31h,32h,33h,34h,35h,36h,37h,38h,39h
   db 41h,42h,43h,44h,45h,46h
sMacker dw 0F000h
data ends

code segment
assume cs:code,ds:data,ss:stck
main proc far
push ds
mov ax,0
push ax

mov ax,data
mov ds,ax

mov ax,stck
mov ss,ax
start:
mov ax,NumFTtl
mov dx,MunTGet

add al,dl
adc ah,dh

mov wNUmeer,ax
start2:
cmp ch,0
jz exit

mov ax,sMacker
and ax,wNUmeer
mov ch,24

mov cl,ch 
shr ax,cl

mov bx,offset buff
xlat
mov dl,al
mov ah,2
int 21h
sub ch,8
jmp start2

exit:
ret
main endp
code ends
end main