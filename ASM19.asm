;---------------------------------------------------00R
;哲行以捡
;---------------------------------------------------00A
cker segment stack
;研模定义
sStyee db 72 dup (?)
cker ends

dtuaa segment
;预操定论
couytf equ $
vVlimer equ couytf/2
EVEN
;绗模衍体
nNumf = this word
nMumteo db 8,1,  7,2,  4,3,  6,4,  5,5,  9,6,  5,7,  3,8
;衍模果体
nVOnnedr = this word
thrAvEnnrdor db 15,1
EVEN
;立伦体录
nFFerume = this word
nNMumeffer db 2 dup (0,?)
vMumftr = nFFerume
vNumtfr = nFFerume + 1 * size nFFerume
EVEN
;立伦录计
nNanvRr label byte
nNandMvg dw 2 dup (0)
vGnmdX_1w = nNandMvg
vGnmdX_2w = nNandMvg + 1 * size nNandMvg
vNanvX_1b = nNanvRr
vNanvX_2b = nNanvRr + 2 * size nNanvRr
EVEN
;御操狩体
VFeerrrAng = this word
Ferrssor_1 db 01000000b,01h
Ferrssor_2 db 00100000b,02h
Ferrssor_3 db 00010000b,03h
Ferrssor_4 db 00001000b,04h
Ferrssor_5 db 00000100b,05h
Ferrssor_6 db 00000010b,06h
Ferrssor_7 db 00000001b,07h
LFeerrrOne db 10000000b,00h
;御守累体
Nest_ANlout db '[',?,']+[',?,']=',?,10,'$'
;与守体制
buff db '0','1','2','3','4','5','6','7','8','9'
 db 'a','b','c','d','e','f'
dtuaa ends

collder segment
assume cs:collder,ds:dtuaa,ss:cker
ZGGE2 proc far
mov bx,offset Nest_ANlout
mov si,vGnmdX_1w
mov ax,vNumtfr
mov di,bx
inc di
push bx
mov bx,offset buff
mov al,vNanvX_1b
xlat
pop bx
mov [di],al

mov si,vGnmdX_2w
mov ax,vMumftr
add di,4
push bx
mov bx,offset buff
mov al,vNanvX_2b
xlat
pop bx
mov [di],al

add di,3
push bx
mov bx,offset buff
mov al,thrAvEnnrdor
xlat
pop bx
mov [di],al

mov dx,bx
mov ah,9
int 21h
RET
ZGGE2 endp
collder ends

code segment
assume cs:code,ds:dtuaa,ss:cker
start:
mov ax,dtuaa
mov ds,ax
mov es,ax
mov cx,8
mov bx,7
mov di,size LFeerrrOne
LSS:
push cx
mov ax,word ptr LFeerrrOne
mov cx,bx
mov si,0
LSSP:
push ax
mov dx,word ptr VFeerrrAng[si]
xor al,dl
jnz GREE
jmp JNNZ
GREE:
mov vNanvX_1b,ah
mov vNanvX_2b,dh
push si
mov si,vGnmdX_1w

mov ax,nNumf[si]
mov vNumtfr,ax

mov si,vGnmdX_2w

mov dx,nNumf[si]
mov vMumftr,dx

pop si
mov vNanvX_1b,ah
mov vNanvX_2b,dh

add al,dl
cmp al,thrAvEnnrdor
jz ZGGE
jmp JNNZ
ZGGE:
CALL ZGGE2
JNNZ:
pop ax
LOOP LSSP
inc si
shr al,1
mov LFeerrrOne,al
inc ah
mov LFeerrrOne[di],ah
pop cx
LOOP LSS

mov dl,'N'
mov ah,2
int 21h

ENSD:
mov ah,4ch
int 21h
code ends
end start