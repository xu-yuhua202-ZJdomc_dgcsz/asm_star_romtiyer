code segment
assume cs:code,ds:code
Branchn = This Dword
branch label word
branchr1 dw r1,2
branchr2 dw r2,4
branchr3 dw r3,6
branchr4 dw r4,8
eniYiy label byte
eniybw equ 'Ozr.662j'
buff db '0','1','2','3','4','5','6','7','8','9'
   db 'a','b','c','d','e','f'
 db eniybw
org 100h
main proc far
push ds
mov ax,0
push ax

mov ax,code
mov ds,ax
start:
mov ah,7
int 21h

cmp al,31h
jl exit
cmp al,34h
jg exit

mov dl,al
mov bl,al
sub bl,31h

mov cl,2
shl bl,cl
mov bh,0
jmp branch[bx]
r1:
mov bx,offset branchr1
mov ax,word ptr ds:[bx][size Branchn*0+2]
mov dl,2
mul dl
mov bx,offset buff
xlat
mov dl,al
mov ah,2
int 21h
jmp start
r2:
mov bx,offset branchr2
mov ax,word ptr ds:[bx][size Branchn*1+2]
mov dl,2
mul dl
mov bx,offset buff
xlat
mov dl,al
mov ah,2
int 21h
jmp start
r3:
mov bx,offset branchr3
mov ax,word ptr ds:[bx][size Branchn*2+2]
mov dl,2
mul dl
mov bx,offset buff
xlat
mov dl,al
mov ah,2
int 21h
jmp start
r4:
mov bx,offset branchr4
mov ax,word ptr ds:[bx][size Branchn*3+2]
mov dl,2
mul dl
mov bx,offset buff
xlat
mov dl,al
mov ah,2
int 21h
jmp start

exit:
mov dl,'o'
mov ah,2
int 21h

ret
main endp
code ends
end main