stck segment stack
stkf db 14 dup (0)
stck ends

codin_g segment
    string db 'a star in G..$'
codin_g ends

star segment
assume cs:star,ds:codin_g,ss:stck
start:
    mov ax,codin_g
    mov ds,ax
    mov es,ax
    mov dx,offset string
    mov di,dx
    mov al,es:[di]
    mov dl,al

    mov al,2
    mov ah,al
    int 21h
    
    mov ah,4ch
    int 21h
star ends
end start